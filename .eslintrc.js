module.exports = {
  extends: [
    // add more generic rulesets here, such as:
    // 'plugin:vue/recommended',
    "plugin:vue/essential",
    "eslint:recommended"
  ],
  rules: {
    "no-console": "off",
    "no-undef": "off",
    "no-unused-vars": "off"
  }
};
