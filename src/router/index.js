import Vue from 'vue'
import Router from 'vue-router'
import VueAgile from 'vue-agile'

// STATIC SITE
import StaticPage from '@/pages/static/Static'
import AboutUs from "@/pages/static/AboutUs";
import ContactUs from "@/pages/static/ContactUs";
import HowTo from "@/pages/static/HowTo";
import PrivacyPolicy from "@/pages/static/PrivacyPolicy";
import Sitemap from "@/pages/static/Sitemap";
import Terms from "@/pages/static/Terms";


// FORUM SITE
import ForumPage from '@/pages/forum/Forum'
import ForumList from '@/pages/forum/ForumList'
import ForumDetail from '@/pages/forum/ForumDetail'

import MessagePage from '@/pages/message/Message'
import MessageList from '@/pages/message/MessageList'


// AUTH PAGES
import LoginPage from '@/pages/general/auth/Login'
import RegisterPage from '@/pages/general/auth/Register'
import ForgotPasswordPage from '@/pages/general/auth/ForgotPassword'
import UserActivationPage from '@/pages/general/auth/UserActivation'
import ResetPasswordPage from '@/pages/general/auth/ResetPassword'


// COMMERCE SITE
import CommercePage from '@/pages/commerce/Commerce'
import HomePage from '@/pages/commerce/Home'
import CommerceSearchPage from '@/pages/commerce/Search'
import ProductCheckoutPage from '@/pages/commerce/product/checkout/ProductCheckout'
import ProductCreatePage from '@/pages/commerce/product/ProductCreate'
import ProductDetailPage from '@/pages/commerce/product/ProductDetail'
import CategoryDetailPage from '@/pages/commerce/CategoryDetail'
import PromoDetailPage from '@/pages/commerce/PromoDetail'
import ProfileDetailPage from '@/pages/commerce/profile/ProfileDetail'
import ProfileUpdatePage from '@/pages/commerce/profile/ProfileUpdate'
import MyStorePage from '@/pages/commerce/store/MyStore'
import StoreCreatePage from '@/pages/commerce/store/StoreCreate'
import StoreDetailPage from '@/pages/commerce/store/StoreDetail'
import MyBankPage from '@/pages/commerce/MyBank'
import WishlistPage from '@/pages/commerce/Wishlist'
import ShipmentPage from '@/pages/commerce/Shipment'
import TransactionPurchasePage from '@/pages/commerce/transaction/TransactionPurchase'
import TransactionSalesPage from '@/pages/commerce/transaction/TransactionSales'
import TransactionSuccessPage from '@/pages/commerce/transaction/TransactionSuccess'
import BidHistory from '@/pages/commerce/BidHistory'
import StoreEdit from '@/pages/commerce/store/StoreEdit'
import ChangePassword from '@/pages/commerce/profile/ChangePassword'
import Rating from '@/pages/commerce/rating/Rating'
import Message from '@/pages/commerce/message/Message'

import ReviewRatingPage from '@/pages/commerce/rating/RatingReview'
// AUCTION SITE
import AuctionPage from '@/pages/commerce/auction/Auction'
import AuctionProduct from '@/pages/commerce/auction/AuctionProduct'
import CategoryAuctionDetail from '@/pages/commerce/auction/CategoryAuctionDetail'
import SearchAuction from '@/pages/commerce/auction/SearchAuction'

// AUCTION SITE
import PaymentPage from '@/pages/commerce/payment/Payment'
import PaymentDone from '@/pages/commerce/payment/PaymentDone'
import PaymentError from '@/pages/commerce/payment/PaymentError'
import PaymentUnfinish from '@/pages/commerce/payment/PaymentUnfinish'


Vue.use(Router);
Vue.use(VueAgile)

export default new Router({
  mode: 'history',
  routes: [
    // STATIC SITE
    {
      path: '/static',
      component: StaticPage,
      children: [
        {
          path: '',
          name: 'Static',
          redirect: { name: 'HomePage'}
        },
        {
          path: 'about-us',
          name: 'StaticAboutUs',
          component: AboutUs,
          props: true
        },
        {
          path: 'contact-us',
          name: 'StaticContactUs',
          component: ContactUs,
          props: true
        },
        {
          path: 'how-to',
          name: 'StaticHowTo',
          component: HowTo,
          props: true
        },
        {
          path: 'privacy-policy',
          name: 'StaticPrivacyPolicy',
          component: PrivacyPolicy,
          props: true
        },
        {
          path: 'sitemap',
          name: 'StaticSitemap',
          component: Sitemap,
          props: true
        },
        {
          path: 'terms-and-conditions',
          name: 'StaticTOC',
          component: Terms,
          props: true
        },
      ]
    },
    // FORUM SITE
    {
      path: '/forum',
      component: ForumPage,
      children: [
        {
          path: '',
          name: 'ForumPage1',
          redirect: '/forum/page/1'
        },
        {
          path: 'page/:page',
          name: 'ForumList',
          component: ForumList
        },
        {
          path: 'detail/:id',
          name: 'ForumDetail',
          component: ForumDetail
        }
      ]
    },
    // MESSAGE SITE
    {
      path: '/message',
      component: MessagePage,
      children: [
        {
          path: 'list',
          name: 'MessageList',
          component: MessageList
        },
      ]
    },
    //AUCTION
    {
      path: '/auction',
      component: AuctionPage,
      children: [
        {
          path: 'product',
          name: 'AuctionProduct',
          component: AuctionProduct
        },
        {
          path: 'category/:id',
          name: 'CategoryAuctionDetail',
          component: CategoryAuctionDetail
        },
        {
          path: 'search',
          name: 'SearchAuction',
          component: SearchAuction
        },
      ]
    },
    //PAYMENT
    {
      path: '/payment',
      component: PaymentPage,
      children: [
        {
          path: 'finish',
          name: 'PaymentDone',
          component: PaymentDone
        },
        {
          path: 'unfinish',
          name: 'PaymentUnfinish',
          component: PaymentUnfinish
        },
        {
          path: 'error',
          name: 'PaymentError',
          component: PaymentError
        },
      ]
    },
    // AUTH PAGE
    {
      path: '/register',
      component: RegisterPage,
    },
    {
      path: '/login',
      component: LoginPage,
    },
    {
      path: '/forgot-password',
      component: ForgotPasswordPage,
    },
    {
      path: '/user-activation',
      component: UserActivationPage,
    },
    {
      path: '/reset-password',
      component: ResetPasswordPage,
    },
    // COMMERCE SITE
    {
      path: '/',
      component: CommercePage,
      children: [
        {
          path: '',
          name: 'CommerceHomePage',
          component: HomePage
        },
        {
          path: '/search',
          name: 'CommerceSearch',
          component: CommerceSearchPage
        },
        {
          path: '/product/checkout',
          name: 'CommerceProductCheckout',
          component: ProductCheckoutPage
        },
        {
          path: '/product/add',
          name: 'CommerceProductCreate',
          component: ProductCreatePage
        },
        {
          path: '/product/:id/update',
          name: 'CommerceProductUpdate',
          component: ProductCreatePage
        },
        {
          path: '/product/:slug',
          name: 'CommerceProductDetail',
          component: ProductDetailPage
        },
        {
          path: '/category/:id',
          name: 'CommerceCategoryDetail',
          component: CategoryDetailPage
        },
        {
          path: '/promo/:id',
          name: 'CommercePromoDetail',
          component: PromoDetailPage
        },
        {
          path: '/profile',
          name: 'CommerceProfileDetail',
          component: ProfileDetailPage
        },
        {
          path: '/profile/update',
          name: 'CommerceProfileUpdate',
          component: ProfileUpdatePage
        },
        {
          path: '/store',
          name: 'CommerceMyStore',
          component: MyStorePage
        },
        {
          path: '/store/create',
          name: 'CommerceStoreCreate',
          component: StoreCreatePage
        },
        {
          path: '/store/edit',
          name: 'StoreEdit',
          component: StoreEdit
        },
        {
          path: '/store/:slug',
          name: 'CommerceStoreDetail',
          component: StoreDetailPage
        },
        {
          path: '/bank',
          name: 'CommerceMyBank',
          component: MyBankPage
        },
        {
          path: '/wishlist',
          name: 'CommerceWishlist',
          component: WishlistPage
        },
        {
          path: '/shipment',
          name: 'CommerceShipment',
          component: ShipmentPage
        },
        {
          path: '/transaction',
          name: 'CommerceTransaction',
          redirect: '/transaction/purchase'
        },
        {
          path: '/transaction/purchase',
          name: 'CommerceTransactionPurchase',
          component: TransactionPurchasePage
        },
        {
          path: '/rating-review',
          name: 'CommerceRatingReview',
          component: ReviewRatingPage
        },
        {
          path: '/transaction/sales',
          name: 'CommerceTransactionSales',
          component: TransactionSalesPage
        },
        {
          path: '/transaction/success',
          name: 'CommerceTransactionSuccess',
          component: TransactionSuccessPage
        },
        {
          path: '/bid/history',
          name: 'BidHistory',
          component: BidHistory
        },
        {
          path: '/change-password',
          component: ChangePassword,
        },
        {
          path: '/rating',
          name: 'Rating',
          component: Rating
        },
        {
          path: '/messages',
          name: 'Messsages',
          component: Message
        },
      ]
    },
    // RANDOM URL THAT DOES NOT MATCH ANY ROUTE ABOVE
    {
      // should be use to redirect to 404 
      path: '*',
      redirect: '/'
    }
  ]
})
