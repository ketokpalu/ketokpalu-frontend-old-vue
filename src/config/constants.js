var api     = ''
var baseUrl = ''
var oneSignalAppId = ''
var domainUrl = ''

switch (process.env.NODE_ENV) {
  case 'production':
    api     = 'https://api.ketokpalu.com'
    baseUrl = 'https://ketokpalu.com'
    domainUrl = 'https://ketokpalu.com'
    oneSignalAppId = '7652a416-74cf-4c4d-88a8-94384109e1f5'
    break;
  case 'local-prod':
    api     = 'https://api.ketokpalu.com'
    baseUrl = 'http://localhost:8080'
    break;
  case 'staging':
    api     = 'http://103.41.205.122:8002'
    baseUrl = 'http://103.41.205.122:8003'
    domainUrl = 'http://stagging.ketokpalu.com'
    oneSignalAppId = '5158ce2f-792e-4e31-89d6-f399c331100a'
    break;
  default:
    api     = 'http://localhost:8002'
    baseUrl = 'http://localhost:8080'
    domainUrl = 'http://localhost:8080'
    break;
}

export {api, baseUrl,oneSignalAppId,domainUrl}
export const cdnImage = 'https://banner.s3-id-jkt-1.kilatstorage.id/'
export const googleClientId = '822777816508-3f9cbc4c7a4knpt1re6fqu62m4ct0stp.apps.googleusercontent.com'
