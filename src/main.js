import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import { domainUrl } from "@/config/constants";

Vue.config.productionTip = false;


// GLOBAL MIXIN DOWN HERE

Vue.mixin({
  methods: {
    loadingShow: function() {
      this.$nextTick(function() {
        $(".loading").fadeIn();
      });
    },
    loadingHide: function() {
      this.$nextTick(function() {
        $(".loading").fadeOut();
      });
    },
    getServerTimeRequest : function() {
      var xmlHttp = new XMLHttpRequest();

      xmlHttp.open('HEAD',domainUrl+'?date='+new Date().getTime(),false);
      xmlHttp.setRequestHeader("Content-Type", "text/html");
      xmlHttp.send('');
      return xmlHttp.getResponseHeader("Date");
    },
    getServerTime : function(){
      var serverTime = this.getServerTimeRequest();

      // console.log(serverTime);
      // var date = new Date(serverTime);

      var date = moment(serverTime);

      return date;
    },
    mounted: function(){
      this.$ga.page(window.location.href);
    }
  }
});



// GLOBAL FILTER DOWN HERE

// import moment from "moment";
// const moment = require('moment-timezone');
import moment from 'moment-timezone';
import VueMobileDetection from 'vue-mobile-detection'
moment.tz.setDefault('Asia/Jakarta');
moment.tz(moment(), 'Asia/Jakarta').format('DD/MM/YYYY HH:mm')
Vue.filter('date', function (value, format = 'DD/MM/YYYY') {
	if (value) {
		return moment(String(value)).format(format);
	}
});

Vue.filter('cut', function (value, endIndex) {
	let newValue = value.substring(0, endIndex)
	if (value.length > endIndex) {
		newValue += '...'
	}
	return newValue
});

Vue.filter('stripTagsAndLimit', function (value, endIndex) {
  let htmlStringTag = value.replace(/(<([^>]+)>)/ig,"");
  let newValue = htmlStringTag.substring(0, endIndex)
  if (htmlStringTag.length > endIndex) {
    newValue += '...'
  }
  return newValue
});



// APP PLUGINS DOWN HERE

var VueCookie = require("vue-cookie");
Vue.use(VueCookie);

var SocialSharing = require('vue-social-sharing');
Vue.use(SocialSharing);

import VueAnalytics from 'vue-analytics';

Vue.use(VueAnalytics, {
  id: 'UA-161533354-1',
});

Vue.use(VueMobileDetection)

/**
 * custom extension so that enable app to flush cookie
 * dependency : tiny cookie
 */
import { getAllCookies, removeCookie } from "tiny-cookie";
Vue.prototype.$cookie.flush = function() {
  const cookieKeys = Object.keys(getAllCookies());
  cookieKeys.forEach(element => {
    removeCookie(element);
  });
};

// number format and currency formating
import VueCurrencyFilter from "vue-currency-filter";
Vue.use(VueCurrencyFilter, {
	symbol: "Rp.",
	thousandsSeparator: ".",
	fractionCount: 0,
	fractionSeparator: ",",
	symbolPosition: "front",
	symbolSpacing: true
});

import VueResource from "vue-resource";
Vue.use(VueResource);



// VUE INSTANCE
new Vue({
  render: h => h(App),
  router
}).$mount("#app");
